package ru.namolem.assignment.github.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.namolem.assignment.github.ui.mainactivity.MainActivity

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(
        modules = [
            FragmentProviderModule::class
        ]
    )
    abstract fun bindMainActivity(): MainActivity
}
