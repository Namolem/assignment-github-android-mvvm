package ru.namolem.assignment.github.ui.profile

import java.util.Date

data class ProfileUiModel(
    val isLoading: Boolean,
    val error: Throwable?,

    val userName: String,
    val createDate: Date?,
    val avatarUrl: String
)
