package ru.namolem.assignment.github.ui.repository.list.repositories.model

data class RepositoryUiModel(
    val id: Int,
    val ownerName: String,
    val description: String?,
    val repositoryName: String
)
