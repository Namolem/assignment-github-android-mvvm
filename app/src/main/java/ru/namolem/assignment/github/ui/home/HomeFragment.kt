package ru.namolem.assignment.github.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_home.*
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.profile.ProfileFragment
import ru.namolem.assignment.github.ui.repository.list.repositories.view.RepositoriesFragment

class HomeFragment : Fragment() {
    init {
        retainInstance = true
    }

    companion object {
        const val TAG = "HomeFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    val profileFragment = ProfileFragment()
    val repositoriesFragment = RepositoriesFragment()
    var activeFragment: Fragment = repositoriesFragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (childFragmentManager.findFragmentByTag(ProfileFragment.TAG) == null) {
            childFragmentManager
                .beginTransaction()
                .add(R.id.containerHome, profileFragment, ProfileFragment.TAG)
                .hide(profileFragment)
                .commit()
        }
        if (childFragmentManager.findFragmentByTag(RepositoriesFragment.TAG) == null) {
            childFragmentManager
                .beginTransaction()
                .add(R.id.containerHome, repositoriesFragment, RepositoriesFragment.TAG)
                .commit()
        }
        vBottomNavigation.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.profileFragment -> {

                    childFragmentManager.beginTransaction()
                        .hide(activeFragment)
                        .show(profileFragment)
                        .commit()
                    activeFragment = profileFragment
                    true
                }
                R.id.repositoriesFragment -> {
                    childFragmentManager.beginTransaction()
                        .hide(activeFragment)
                        .show(repositoriesFragment)
                        .commit()
                    activeFragment = repositoriesFragment
                    true
                }

                else -> false
            }
        }
    }
}
