package ru.namolem.assignment.github.ui.base.recycler.networkstate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import ru.namolem.assignment.github.R

class NetworkStateViewHolder(
    view: View,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(view) {

    private val progressBar = view.findViewById<ProgressBar>(R.id.progress_bar)
    private val retry = view.findViewById<Button>(R.id.retry_button)
    private val errorMsg = view.findViewById<TextView>(R.id.error_msg)

    init {
        retry.setOnClickListener { retryCallback() }
    }

    fun bind(networkState: NetworkState?) {
        progressBar.isVisible = networkState?.status == Status.RUNNING
        retry.isVisible = networkState?.status == Status.FAILED
        errorMsg.isVisible = networkState?.msg != null
        errorMsg.text = networkState?.msg
    }


    companion object {

        fun create(parent: ViewGroup, retryCallback: () -> Unit): NetworkStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_network_state, parent, false)

            return NetworkStateViewHolder(
                view,
                retryCallback
            )
        }

    }
}

