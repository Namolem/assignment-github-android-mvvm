package ru.namolem.assignment.github.ui.repository.list.repositories.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_repositories.view.*
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.di.ViewModelProviderFactory
import ru.namolem.assignment.github.ui.base.recycler.networkstate.NetworkState
import ru.namolem.assignment.github.ui.mainactivity.MainActivity
import ru.namolem.assignment.github.ui.repository.list.repositories.RepositoriesViewModel
import ru.namolem.assignment.github.ui.repository.list.repositories.view.adapter.RepositoriesAdapter
import timber.log.Timber
import javax.inject.Inject

class RepositoriesFragment : Fragment() {

    init {
        retainInstance = true
    }

    companion object {
        const val TAG = "RepositoriesFragment"
    }

    @Inject lateinit var viewModelProviderFactory: ViewModelProviderFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        AndroidSupportInjection.inject(this)
        val view = inflater.inflate(R.layout.fragment_repositories, container, false)
        val viewModel = ViewModelProviders.of(this, viewModelProviderFactory)
            .get(RepositoriesViewModel::class.java)
        Timber.e("viewModel: ${viewModel.toString()}")
        subscribeUi(view, viewModel)
        return view
    }

    private fun subscribeUi(view: View, viewModel: RepositoriesViewModel) {
        val repositoriesAdapter = RepositoriesAdapter(
            retryCallback = { viewModel.retry() }
        )

        repositoriesAdapter.itemClickListener = { repository ->
            (activity as MainActivity).goToRepository(repository)
        }

        view.rvRepositories.adapter = repositoriesAdapter
        view.rvRepositories.layoutManager = LinearLayoutManager(view.context)

        view.rvRepositories.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))

        viewModel.posts.observe(this, Observer {
            Timber.e("Got new posts[${it.size}]")
            repositoriesAdapter.submitList(it)
        })
        viewModel.networkState.observe(this, Observer {
            repositoriesAdapter.setNetworkState(it)
        })

        viewModel.refreshState.observe(this, Observer {
            view.layoutSwipeRefresh.isRefreshing = it == NetworkState.LOADING
        })
        view.layoutSwipeRefresh.setOnRefreshListener {
            viewModel.refresh()
        }
        view.loadingView.isVisible = false
        view.rvRepositories.isGone = false
    }

}


