package ru.namolem.assignment.github.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel
import ru.namolem.assignment.github.ui.repository.list.repositories.usecase.FetchRepositoriesUseCase
import java.util.concurrent.Executor


class RepositoriesSourceFactory(
    private val retryExecutor: Executor,
    private val fetchRepositoriesUseCase: FetchRepositoriesUseCase
) : DataSource.Factory<Int, RepositoryUiModel>() {

    val sourceLiveData = MutableLiveData<PageKeyedRepositoriesDataSource>()

    override fun create(): DataSource<Int, RepositoryUiModel> {
        val source = PageKeyedRepositoriesDataSource(
            retryExecutor,
            fetchRepositoriesUseCase
        )
        sourceLiveData.postValue(source)
        return source
    }
}
