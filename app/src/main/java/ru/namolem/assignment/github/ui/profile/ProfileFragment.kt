package ru.namolem.assignment.github.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile.view.*
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.di.ViewModelProviderFactory
import ru.namolem.assignment.github.ui.base.BaseFragment
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

class ProfileFragment : BaseFragment() {

    init {
        retainInstance = true
    }

    companion object {
        const val TAG = "ProfileFragment"
    }

    @Inject lateinit var viewModelProviderFactory: ViewModelProviderFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        AndroidSupportInjection.inject(this)
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        val viewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(ProfileViewModel::class.java)
        subscribeUi(view, viewModel)
        return view
    }

    private fun subscribeUi(
        view: View,
        viewModel: ProfileViewModel
    ) {
        viewModel.uiModel.observe(viewLifecycleOwner, Observer { uiModel: ProfileUiModel ->
            view.tvUserName.text = uiModel.userName

            Glide.with(view)
                .load(uiModel.avatarUrl)
                .into(view.ivPhoto)

            view.tvDate.text = uiModel.createDate?.let {
                SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(uiModel.createDate)
            } ?: ""

        })
    }
}
