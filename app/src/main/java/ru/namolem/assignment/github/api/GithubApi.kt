package ru.namolem.assignment.github.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query
import ru.namolem.assignment.github.api.model.dto.RepositoryDto
import ru.namolem.assignment.github.api.model.dto.UserInfoDto

interface GithubApi {
    @GET("repositories")
    fun getRepositories(
        @Query("since") since: Int?,
        @Header("Authorization") token: String = "token e28e918f480b08a4e4bd6e1952348dc5e57178b0"
    ): Single<Response<List<RepositoryDto>>>

    @GET("/users/{name}")
    fun getUserInfo(
        @Path("name") name: String,
        @Header("Authorization") token: String = "token e28e918f480b08a4e4bd6e1952348dc5e57178b0"
    ): Single<UserInfoDto>
}
