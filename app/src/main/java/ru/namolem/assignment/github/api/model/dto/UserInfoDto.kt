package ru.namolem.assignment.github.api.model.dto

import com.google.gson.annotations.SerializedName

class UserInfoDto(
    @SerializedName("login") val login: String,
    @SerializedName("avatar_url") val avatarUrl: String,
    @SerializedName("created_at") val createdAt: String

)
