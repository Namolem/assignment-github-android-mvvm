package ru.namolem.assignment.github.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.namolem.assignment.github.api.GithubApi
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Locale

class ProfileViewModel(private val githubApi: GithubApi) : ViewModel() {
    private val disposables = CompositeDisposable()
    val uiModel: MutableLiveData<ProfileUiModel> = MutableLiveData()

    init {
        uiModel.value = ProfileUiModel(
            isLoading = false,
            error = null,
            avatarUrl = "",
            createDate = null,
            userName = ""
        )
        githubApi.getUserInfo("Namolem")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                uiModel.setValue(
                    uiModel.value?.copy(
                        isLoading = true
                    )
                )
            }
            .doOnSuccess {
                uiModel.setValue(
                    uiModel.value?.copy(
                        isLoading = false,
                        userName = it.login,
                        avatarUrl = it.avatarUrl,
                        createDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.getDefault())
                            .parse(it.createdAt)
                    )
                )
            }
            .doOnError { Timber.e(it) }
            .toCompletable()
            .onErrorComplete()
            .subscribe()
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}

