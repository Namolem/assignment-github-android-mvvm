package ru.namolem.assignment.github.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.namolem.assignment.github.GithubApp
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ActivityBuilder::class
    ]
)
interface AppComponent {
    fun inject(app: GithubApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: GithubApp): Builder

        fun build(): AppComponent
    }
}
