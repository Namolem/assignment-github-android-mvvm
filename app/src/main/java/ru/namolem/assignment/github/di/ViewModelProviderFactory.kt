package ru.namolem.assignment.github.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.namolem.assignment.github.api.GithubApi
import ru.namolem.assignment.github.repository.InMemoryReposRepository
import ru.namolem.assignment.github.ui.profile.ProfileViewModel
import ru.namolem.assignment.github.ui.repository.list.repositories.RepositoriesViewModel
import javax.inject.Inject

class ViewModelProviderFactory @Inject constructor(
    private val githubApi: GithubApi,
    private val reposRepository: InMemoryReposRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return when {

            modelClass.isAssignableFrom(ProfileViewModel::class.java) ->
                ProfileViewModel(githubApi) as T

            modelClass.isAssignableFrom(RepositoriesViewModel::class.java) ->
                RepositoriesViewModel(reposRepository) as T

            else -> throw IllegalArgumentException("Unknown ViewModel class ${modelClass.simpleName}")
        }
    }

}
