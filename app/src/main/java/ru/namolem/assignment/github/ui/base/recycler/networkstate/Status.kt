package ru.namolem.assignment.github.ui.base.recycler.networkstate

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}
