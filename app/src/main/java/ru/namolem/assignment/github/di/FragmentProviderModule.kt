package ru.namolem.assignment.github.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.namolem.assignment.github.ui.profile.ProfileFragment
import ru.namolem.assignment.github.ui.repository.list.repositories.view.RepositoriesFragment
import ru.namolem.assignment.github.ui.repository.details.RepositoryDetailsFragment

@Module
abstract class FragmentProviderModule {
    @ContributesAndroidInjector abstract fun provideProfileFragment(): ProfileFragment
    @ContributesAndroidInjector abstract fun provideRepositoriesFragment(): RepositoriesFragment
    @ContributesAndroidInjector abstract fun provideRepositoryFragment(): RepositoryDetailsFragment
}
