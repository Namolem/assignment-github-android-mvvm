package ru.namolem.assignment.github.ui.repository.list.repositories.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_repository.view.*
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel

class RepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(item: RepositoryUiModel?) {
        itemView.tvOwnerName.text = item?.ownerName
        itemView.tvRepositoryName.text = item?.repositoryName
    }

    companion object {
        fun create(parent: ViewGroup): RepositoryViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repository, parent, false)
            return RepositoryViewHolder(view)
        }
    }
}
