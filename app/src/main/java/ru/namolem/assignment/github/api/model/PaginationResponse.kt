package ru.namolem.assignment.github.api.model

data class PaginationResponse<T>(
    val data: T,
    val nextPage: Int?
)
