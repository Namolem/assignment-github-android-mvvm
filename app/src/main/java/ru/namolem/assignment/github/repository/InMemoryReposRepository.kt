package ru.namolem.assignment.github.repository

import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import ru.namolem.assignment.github.ui.base.model.Listing
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel
import ru.namolem.assignment.github.ui.repository.list.repositories.usecase.FetchRepositoriesUseCase
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InMemoryReposRepository @Inject constructor(
    private val fetchRepositoriesUseCase: FetchRepositoriesUseCase
) {
    private val networkExecutor = Executors.newFixedThreadPool(5)
    var cachedItems: List<RepositoryUiModel> = emptyList()
        private set

    fun reposListing(): Listing<RepositoryUiModel> {
        val sourceFactory = RepositoriesSourceFactory(
            networkExecutor,
            fetchRepositoriesUseCase
        )
        val livePagedList = sourceFactory.toLiveData(
            pageSize = 300,
            fetchExecutor = networkExecutor
        )
        livePagedList.observeForever {
            cachedItems = (cachedItems + it).distinctBy { it.id }
        }

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }

        return Listing(
            pagedList = livePagedList,
            networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                it.networkState
            },
            retry = {
                sourceFactory.sourceLiveData.value?.retryAllFailed()
            },
            refresh = {
                sourceFactory.sourceLiveData.value?.invalidate()
            },
            refreshState = refreshState
        )
    }
}
