package ru.namolem.assignment.github.ui.repository.list.repositories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import ru.namolem.assignment.github.repository.InMemoryReposRepository

class RepositoriesViewModel(
    reposRepository: InMemoryReposRepository
) : ViewModel() {
    private val disposables = CompositeDisposable()

    private val repoResult = MutableLiveData(reposRepository.reposListing())
    val posts = switchMap(repoResult) { it.pagedList }
    val networkState = switchMap(repoResult) { it.networkState }
    val refreshState = switchMap(repoResult) { it.refreshState }

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun retry() {
        val listing = repoResult.value
        listing?.retry?.invoke()
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}

