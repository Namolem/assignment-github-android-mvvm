package ru.namolem.assignment.github.ui.repository.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_repository_details.view.*
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel

class RepositoryDetailsFragment : Fragment() {

    companion object {
        const val KEY_REPO_NAME = "repositoryName"
        const val KEY_REPO_DESCRIPTION = "repositoryDescription"
        const val KEY_REPO_OWNER = "repositoryOwner"

        fun newInstance(repository: RepositoryUiModel): RepositoryDetailsFragment {
            return RepositoryDetailsFragment().apply {
                arguments = bundleOf(
                    KEY_REPO_NAME to repository.repositoryName,
                    KEY_REPO_OWNER to repository.ownerName,
                    KEY_REPO_DESCRIPTION to repository.description

                )
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        AndroidSupportInjection.inject(this)


        val ownerName = arguments?.getString(KEY_REPO_OWNER, "")
        val name = arguments?.getString(KEY_REPO_NAME, "")
        val description = arguments?.getString(KEY_REPO_DESCRIPTION, "")

        val view = inflater.inflate(R.layout.fragment_repository_details, container, false)
        view.tvRepositoryName.text = name
        view.tvRepositoryDescription.text = description
        view.tvOwnerName.text = ownerName
        return view
    }

}
