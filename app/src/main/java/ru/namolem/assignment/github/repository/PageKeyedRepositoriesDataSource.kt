package ru.namolem.assignment.github.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.namolem.assignment.github.api.model.dto.toUiModel
import ru.namolem.assignment.github.ui.base.recycler.networkstate.NetworkState
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel
import ru.namolem.assignment.github.ui.repository.list.repositories.usecase.FetchRepositoriesUseCase
import timber.log.Timber
import java.util.concurrent.Executor

class PageKeyedRepositoriesDataSource(
    private val retryExecutor: Executor,
    private val fetchRepositoriesUseCase: FetchRepositoriesUseCase
) : PageKeyedDataSource<Int, RepositoryUiModel>() {

    // function reference for the retry request
    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()
    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, RepositoryUiModel>) {
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)

        fetchRepositoriesUseCase.execute(null)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { t ->
                Timber.e(t)
                retry = { loadInitial(params, callback) }
                networkState.postValue(
                    NetworkState.error(t.message)
                )
            }
            .doOnSuccess { paginationResponse ->
                val data = paginationResponse.data
                val items = data.map { it.toUiModel() }
                retry = null
                networkState.postValue(NetworkState.LOADED)
                initialLoad.postValue(NetworkState.LOADED)
                callback.onResult(items, null, paginationResponse.nextPage)
            }
            .toCompletable()
            .onErrorComplete()
            .subscribe()
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, RepositoryUiModel>) {
        networkState.postValue(NetworkState.LOADING)
        fetchRepositoriesUseCase.execute(
            since = params.key
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { t ->
                Timber.e(t)
                retry = { loadAfter(params, callback) }
                networkState.postValue(
                    NetworkState.error(t.message)
                )
            }
            .doOnSuccess { paginationResponse ->
                val data = paginationResponse.data
                val items = data.map { it.toUiModel() }
                retry = null
                callback.onResult(items, paginationResponse.nextPage)
                networkState.postValue(NetworkState.LOADED)
            }
            .toCompletable()
            .onErrorComplete()
            .subscribe()
    }

    // ignored, since we only ever append to our initial load
    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, RepositoryUiModel>) {}
}
