package ru.namolem.assignment.github.ui.repository.list.repositories.usecase

import io.reactivex.Single
import ru.namolem.assignment.github.api.GithubApi
import ru.namolem.assignment.github.api.model.dto.RepositoryDto
import ru.namolem.assignment.github.api.model.PaginationResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FetchRepositoriesUseCase @Inject constructor(
    private val githubApi: GithubApi
) {

    fun execute(since: Int?): Single<PaginationResponse<List<RepositoryDto>>> {
        return githubApi.getRepositories(since)
            .map { response ->
                if (response.isSuccessful) {
                    val page = response.headers()["Link"]
                        ?.split(',')
                        ?.firstOrNull { it.contains("rel=\"next\"") }
                        ?.split(";")?.first()
                        ?.substringAfter("since=")
                        ?.trimEnd('>')
                        ?.toInt()
                    PaginationResponse(response.body()!!, page)
                } else {
                    throw Exception()
                }
            }
    }
}
