package ru.namolem.assignment.github.ui.mainactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.home.HomeFragment
import ru.namolem.assignment.github.ui.repository.details.RepositoryDetailsFragment
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        if (supportFragmentManager.findFragmentByTag(HomeFragment.TAG) == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.mainContent, HomeFragment(), HomeFragment.TAG)
                .commit()
        }
    }

    fun goToRepository(repository: RepositoryUiModel) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.mainContent, RepositoryDetailsFragment.newInstance(repository))
            .addToBackStack(null)
            .commit()
    }

    override fun onBackPressed() {
        if (!supportFragmentManager.popBackStackImmediate())
            super.onBackPressed()
    }
}
