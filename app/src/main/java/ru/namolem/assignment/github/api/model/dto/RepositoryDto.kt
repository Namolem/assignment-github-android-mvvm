package ru.namolem.assignment.github.api.model.dto

import com.google.gson.annotations.SerializedName
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel

data class RepositoryDto(
    @SerializedName("id") val id: Int,
    @SerializedName("owner") val owner: UserInfoDto,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String?
)

fun RepositoryDto.toUiModel(): RepositoryUiModel {
    return RepositoryUiModel(
        id = id,
        ownerName = owner.login,
        repositoryName = name,
        description = description
    )
}
