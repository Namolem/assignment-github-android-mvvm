package ru.namolem.assignment.github.ui.repository.list.repositories.view.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.base.recycler.networkstate.NetworkState
import ru.namolem.assignment.github.ui.base.recycler.networkstate.NetworkStateViewHolder
import ru.namolem.assignment.github.ui.repository.list.repositories.model.RepositoryUiModel

class RepositoriesAdapter(
    private val retryCallback: () -> Unit
) : PagedListAdapter<RepositoryUiModel, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    private var networkState: NetworkState? = null

    var itemClickListener: ((RepositoryUiModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_repository -> RepositoryViewHolder.create(
                parent
            )
            R.layout.item_network_state -> NetworkStateViewHolder.create(parent, retryCallback)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_repository -> {
                (holder as RepositoryViewHolder).bind(getItem(position))
                holder.itemView.setOnClickListener {
                    itemClickListener?.invoke(getItem(position)!!)
                }

            }
            R.layout.item_network_state -> (holder as NetworkStateViewHolder).bind(networkState)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_network_state
        } else {
            R.layout.item_repository
        }

    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != networkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<RepositoryUiModel>() {
            override fun areItemsTheSame(oldItem: RepositoryUiModel, newItem: RepositoryUiModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: RepositoryUiModel, newItem: RepositoryUiModel): Boolean {
                return oldItem == newItem
            }

        }
    }

}
