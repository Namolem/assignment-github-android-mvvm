package ru.namolem.assignment.github.ui.mainactivity

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ru.namolem.assignment.github.R
import ru.namolem.assignment.github.ui.profile.ProfileFragment
import ru.namolem.assignment.github.ui.repository.list.repositories.view.RepositoriesFragment

class TabsPagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> RepositoriesFragment()
            1 -> ProfileFragment()
            else -> throw IllegalArgumentException()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.title_repositories)
            1 -> context.getString(R.string.title_profile)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getCount() = 2
}
